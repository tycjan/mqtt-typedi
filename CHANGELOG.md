# Changelog

### 0.1.2

-  fix: peer dependencies config

### 0.1.1

-  fix: README.md
-  chore: add CHANGELOG.md
-  chore: remove unnecessary packages
