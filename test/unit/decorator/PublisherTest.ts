import {Container} from 'typedi';
import {Client} from 'mqtt';
import {defaultOptions, Publisher} from '../../../src';

describe('Publisher Test', () => {
   let clientMock: any;

   beforeEach(() => {
      clientMock = {
         subscribe: jest.fn((topic, options, cb) => cb()),
         publish: jest.fn(),
      };
   });

   describe('Publisher subscribe on topic', () => {
      it('should subscribe on topic when decorated class object uses publish method', () => {
         Container.set(Client, clientMock);

         @Publisher('test')
         class TestPublisher {
            publish() {
               return undefined;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish();

         expect(clientMock.subscribe).toHaveBeenCalled();
      });

      it('should subscribe on provided topic when decorated class object uses publish method', () => {
         Container.set(Client, clientMock);
         const topicName = 'test1';

         @Publisher(topicName)
         class TestPublisher {
            publish() {
               return undefined;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish();

         expect(clientMock.subscribe.mock.calls[0][0]).toEqual(topicName);
      });
   });

   describe('Publisher publish on topic', () => {
      it('should publish on topic when decorated class object uses publish method', () => {
         Container.set(Client, clientMock);

         @Publisher('test')
         class TestPublisher {
            publish() {
               return undefined;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish();

         expect(clientMock.publish).toHaveBeenCalled();
      });

      it('should publish on provided topic when decorated class object uses publish method', () => {
         Container.set(Client, clientMock);
         const topicName = 'test2';

         @Publisher(topicName)
         class TestPublisher {
            publish() {
               return undefined;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish();

         expect(clientMock.publish.mock.calls[0][0]).toEqual(topicName);
      });

      it('should publish provided message when decorated class object uses publish method', () => {
         Container.set(Client, clientMock);
         const testMessage = 'test1';

         @Publisher('test')
         class TestPublisher {
            publish(message: string) {
               return message;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish(testMessage);

         expect(clientMock.publish.mock.calls[0][1]).toEqual(testMessage);
      });

      it('should publish provided message on provided topic when decorated class object uses publish method', () => {
         Container.set(Client, clientMock);
         const testMessage = 'test_message';
         const testTopic = 'test_topic';

         @Publisher(testTopic)
         class TestPublisher {
            publish(message: string) {
               return message;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish(testMessage);

         expect(clientMock.publish.mock.calls[0][0]).toEqual(testTopic);
         expect(clientMock.publish.mock.calls[0][1]).toEqual(testMessage);
      });
   });

   describe('Publisher subscribe options', () => {
      it('should publish with default options when no options provided', () => {
         Container.set(Client, clientMock);

         @Publisher('test')
         class TestPublisher {
            publish() {
               return undefined;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish();

         expect(clientMock.subscribe.mock.calls[0][1]).toEqual(defaultOptions);
      });

      it('should publish with options when decorator receives options object', () => {
         Container.set(Client, clientMock);

         @Publisher('test', {
            qos: 2,
         })
         class TestPublisher {
            publish() {
               return undefined;
            }
         }

         const testPublisher = new TestPublisher();
         testPublisher.publish();

         expect(clientMock.subscribe.mock.calls[0][1]).toEqual({qos: 2});
      });
   });
});
