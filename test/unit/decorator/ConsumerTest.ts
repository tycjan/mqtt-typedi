import {Container} from 'typedi';
import {Client} from 'mqtt';
import {Consumer, defaultOptions} from '../../../src';

describe('Consumer Test', () => {
   let clientMock: any;

   beforeEach(() => {
      clientMock = {
         subscribe: jest.fn((topic, options, cb) => cb()),
         on: jest.fn(),
      };
   });

   describe('Consumer subscribe on topic', () => {
      it('should subscribe on topic on decorated class object', () => {
         Container.set(Client, clientMock);

         @Consumer('test')
         class TestConsumer {
            consume() {
               return undefined;
            }
         }
         new TestConsumer();
         expect(clientMock.subscribe).toHaveBeenCalled();
      });

      it('should subscribe on provided topic on decorated class object', () => {
         Container.set(Client, clientMock);
         const testTopic = 'test';

         @Consumer(testTopic)
         class TestConsumer {
            consume() {
               return undefined;
            }
         }
         new TestConsumer();
         expect(clientMock.subscribe.mock.calls[0][0]).toEqual(testTopic);
      });
   });

   describe('Consumer consume message', () => {
      it('should call decorated class object consume method when new message appears on topic', () => {
         clientMock.on = jest.fn((event, cb) => cb('test', Buffer.from('test')));
         Container.set(Client, clientMock);

         @Consumer('test')
         class TestConsumer {
            consume = jest.fn();
         }
         const testConsumer = new TestConsumer();

         expect(testConsumer.consume).toHaveBeenCalled();
      });

      it('should call decorated class object consume method with topic and message when new message appears on topic', () => {
         const testTopic = 'test_topic';
         const testMessage = 'test_message';

         clientMock.on = jest.fn((event, cb) => cb(testTopic, Buffer.from(testMessage)));

         Container.set(Client, clientMock);

         @Consumer('test')
         class TestConsumer {
            consume = jest.fn();
         }
         const testConsumer = new TestConsumer();

         expect(testConsumer.consume.mock.calls[0][0]).toEqual(testTopic);
         expect(testConsumer.consume.mock.calls[0][1]).toEqual(testMessage);
      });
   });

   describe('Consumer subscribe options', () => {
      it('should consume with default options when no options provided', () => {
         Container.set(Client, clientMock);

         @Consumer('test')
         class TestConsumer {
            consume() {
               return undefined;
            }
         }
         new TestConsumer();
         expect(clientMock.subscribe.mock.calls[0][1]).toEqual(defaultOptions);
      });

      it('should consume with options when decorator receives options object', () => {
         Container.set(Client, clientMock);

         @Consumer('test', {
            qos: 2,
         })
         class TestConsumer {
            consume() {
               return undefined;
            }
         }
         new TestConsumer();
         expect(clientMock.subscribe.mock.calls[0][1]).toEqual({qos: 2});
      });
   });
});
