import {Container} from 'typedi';
import {Client, IClientSubscribeOptions} from 'mqtt';
import {defaultOptions} from './defaultSubscribeOptions';

export interface ConsumerInterface {
   consume: (topic: string, message: string) => any;
}

export const Consumer = (topic: string, options?: IClientSubscribeOptions) => <T extends {new (...args): ConsumerInterface}>(
   constructor: T
) =>
   class extends constructor {
      constructor(...args) {
         super(...args);

         const client = Container.get<Client>(Client);

         client.subscribe(topic, options ? options : defaultOptions, () => {
            client.on('message', (topic: string, message: Buffer) => {
               this.consume(topic, message.toString('utf8'));
            });
         });
      }
   };
