import {Container} from 'typedi';
import {Client, IClientSubscribeOptions} from 'mqtt';
import {defaultOptions} from './defaultSubscribeOptions';

export interface PublisherInterface {
   publish: (message: string) => any;
}

export const Publisher = (topic: string, options?: IClientSubscribeOptions) => <T extends {new (...args): PublisherInterface}>(
   constructor: T
) =>
   class extends constructor {
      constructor(...args) {
         super(...args);

         this.publish = (message: string): void => {
            const client = Container.get(Client);
            client.subscribe(topic, options ? options : defaultOptions, () => {
               client.publish(topic, message);
            });
         };
      }
   };
