import {IClientSubscribeOptions} from 'mqtt';

export const defaultOptions: IClientSubscribeOptions = {
   qos: 0,
};
