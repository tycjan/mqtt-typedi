module.exports = {
   preset: 'ts-jest',
   testEnvironment: 'node',
   verbose: true,
   testRegex: '.*\\Test\\.ts$',
   moduleDirectories: ['node_modules', 'src'],
   transformIgnorePatterns: ['node_modules/*'],
};
